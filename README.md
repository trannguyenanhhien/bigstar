# Install
## Backend
- cd backend
- clone .env
- yarn install
- yarn serve
## Frontend
- cd frontend
- clone .env
- yarn install
- yarn start

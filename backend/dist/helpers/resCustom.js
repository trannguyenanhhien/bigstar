"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resCustom = void 0;
const resCustom = (status, data, message) => {
    return {
        success: status === 200 ? true : false,
        message: message,
        status_code: status,
        data: data === null || data === void 0 ? void 0 : data.data,
        page_info: data === null || data === void 0 ? void 0 : data.page_info,
    };
};
exports.resCustom = resCustom;

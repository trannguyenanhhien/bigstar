"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAuthRefresh = exports.checkAuth = exports.createRefreshToken = exports.createToken = void 0;
const jsonwebtoken_1 = __importStar(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
const resCustom_1 = require("./resCustom");
dotenv_1.default.config();
const secretKey = 'hien123';
const refreshSecretKey = 'hien123-retoken';
const createToken = (user) => {
    return jsonwebtoken_1.default.sign({ userId: user._id }, secretKey, {
        expiresIn: '150m',
    });
};
exports.createToken = createToken;
const createRefreshToken = (user) => {
    return jsonwebtoken_1.default.sign({ userId: user._id }, refreshSecretKey, {
        expiresIn: '15000m',
    });
};
exports.createRefreshToken = createRefreshToken;
const checkAuth = (req, res, next) => {
    try {
        const authHeader = req.header('Authorization');
        const accessToken = authHeader && authHeader.split(' ')[1];
        if (!accessToken)
            res.status(401).json((0, resCustom_1.resCustom)(401, {}, 'Token expired'));
        const decodedUser = (0, jsonwebtoken_1.verify)(accessToken, secretKey);
        if (decodedUser)
            return next();
        else
            res.status(401).json((0, resCustom_1.resCustom)(401, {}, 'Token expired'));
    }
    catch (error) {
        throw res.status(500).json({ mess: 'Lỗi rồi 123' });
    }
};
exports.checkAuth = checkAuth;
const checkAuthRefresh = (req, res) => {
    try {
        const tokenReq = req.body.refresh;
        const refreshToken = tokenReq && tokenReq.split(' ')[1];
        if (!refreshToken)
            res.status(401).json((0, resCustom_1.resCustom)(401, {}, 'Refresh token expired'));
        const decodedUser = jsonwebtoken_1.default.verify(refreshToken, refreshSecretKey);
        if (decodedUser)
            return decodedUser;
        else
            res.status(401).json((0, resCustom_1.resCustom)(401, {}, 'Refresh token expired'));
    }
    catch (error) {
        res.status(500).json({ mess: 'Lỗi rồi 12' });
    }
};
exports.checkAuthRefresh = checkAuthRefresh;

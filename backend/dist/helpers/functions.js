"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatISODate = exports.capitalize = exports.compareDate = exports.currentDate = exports.formatDateCustom = exports.formatDateTime = exports.formatDateData = exports.formatDate = exports.formatMoney = void 0;
/* eslint-disable no-useless-escape */
// const moment = require('moment')
const moment_1 = __importDefault(require("moment"));
function formatMoney(n) {
    return (Math.round(n * 100) / 100).toLocaleString();
}
exports.formatMoney = formatMoney;
function formatDate(date, char = '/') {
    return (0, moment_1.default)(date).format(`D${char}M${char}YYYY`);
}
exports.formatDate = formatDate;
function formatDateData(date, char = '/') {
    return (0, moment_1.default)(date).format(`YYYY${char}MM${char}DD`);
}
exports.formatDateData = formatDateData;
function formatDateTime(date, char = '/') {
    return (0, moment_1.default)(date).format(`D${char}M${char}YYYY hh:mm:ss`);
}
exports.formatDateTime = formatDateTime;
function formatDateCustom(date, format) {
    return (0, moment_1.default)(date).format(format);
}
exports.formatDateCustom = formatDateCustom;
function formatISODate(date) {
    return moment_1.default.utc((0, moment_1.default)(date).format('YYYY-MM-DD'));
}
exports.formatISODate = formatISODate;
function currentDate() {
    const date = new Date();
    return date;
}
exports.currentDate = currentDate;
function compareDate(a, b) {
    const date1 = new Date(a).getTime();
    const date2 = new Date(b).getTime();
    if (date1 > date2)
        return 1;
    else if (date1 < date2)
        return 2;
    else
        return 0;
}
exports.compareDate = compareDate;
function strToSlug(title) {
    //Đổi chữ hoa thành chữ thường
    let slug = title.toLowerCase();
    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, '-');
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    return slug;
}
// async function checkToken() {
//   let currentDate = new Date()
//   const token = localStorage.getItem('authToken')
//   if (token) {
//     const decode = await jwt_decode(token)
//     if (decode.exp * 1000 >= currentDate.getTime()) {
//       return true
//     }
//   }
//   return false
// }
// const getTokenH = () => {
//   const Token = localStorage.getItem('authToken')
//   return Token
// }
// const getRefresh = () => {
//   const RefreshToken = state.authSlice.auth.refresh;
//   return RefreshToken;
// };
// const getProfile = () => {
//   const Profile = JSON.parse(localStorage.getItem("profile"));
//   return Profile;
// };
const capitalize = (text) => {
    return text
        .toLowerCase()
        .split(' ')
        .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
        .join(' ');
};
exports.capitalize = capitalize;

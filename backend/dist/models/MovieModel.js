"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MovieModel = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
    },
    created_at: {
        type: Date,
        required: true,
    },
    poster: {
        type: String,
        required: true,
    },
    like: {
        type: Array,
        default: []
    },
    dislike: {
        type: Array,
        default: []
    },
});
exports.MovieModel = mongoose_1.default.model('Movie', schema);

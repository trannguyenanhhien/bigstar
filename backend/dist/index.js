"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
const auth_1 = require("./helpers/auth");
const mongoose_1 = __importDefault(require("mongoose"));
const database_1 = require("./configs/database");
const body_parser_1 = __importDefault(require("body-parser"));
const users_1 = __importDefault(require("./routes/users"));
const movies_1 = __importDefault(require("./routes/movies"));
dotenv_1.default.config();
const port = process.env.PORT;
const app = (0, express_1.default)();
app.use(body_parser_1.default.json({ limit: '30mb' }));
app.use(body_parser_1.default.urlencoded({ extended: true, limit: '30mb' }));
const allowedOrigins = ['http://localhost:3000'];
const corsOptions = {
    origin: allowedOrigins,
};
app.use((0, cors_1.default)(corsOptions));
app.use('/users', users_1.default);
const middlewware = (req, res, next) => {
    (0, auth_1.checkAuth)(req, res, next);
};
app.use('/movies', movies_1.default);
app.use(middlewware);
// mongoose.set('strictQuery', false);
mongoose_1.default
    .connect(database_1.URI, { useNewURLParser: true, useUnifiedTopology: true })
    .then(() => {
    console.log('Connect to DB');
    app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });
})
    .catch((err) => {
    console.log('err', err);
});

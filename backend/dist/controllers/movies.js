"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMovie = exports.rateMovie = exports.getMovies = void 0;
const functions_1 = require("../helpers/functions");
const resCustom_1 = require("../helpers/resCustom");
const MovieModel_1 = require("../models/MovieModel");
const UserModel_1 = require("../models/UserModel");
const getMovies = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = req.query;
        const length = (yield MovieModel_1.MovieModel.find()).length;
        const movies = yield MovieModel_1.MovieModel.find()
            .limit(query.page_size)
            .skip((query.page - 1) * query.page_size);
        const data = {
            data: movies,
            page_info: {
                current_limit: parseInt(query.page_size),
                current_page: parseInt(query.page),
                total: length,
            },
        };
        res.status(200).json((0, resCustom_1.resCustom)(200, { data: data.data, page_info: data.page_info }));
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.getMovies = getMovies;
const rateMovie = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c;
    const { _id, vote, userId } = req.body;
    const existMovie = yield MovieModel_1.MovieModel.find({ _id: _id });
    const existUser = yield UserModel_1.UserModel.find({ _id: userId });
    const votedUser = yield UserModel_1.UserModel.find({
        $and: [
            { _id: userId },
            {
                $or: [{ [vote]: { $all: [_id] } }],
            },
        ],
    });
    let newMovie = existMovie[0];
    let newUser = existUser[0];
    if (votedUser.length) {
        newMovie = Object.assign(Object.assign({}, existMovie[0]._doc), { [vote]: (_a = existMovie[0][vote]) === null || _a === void 0 ? void 0 : _a.filter((i) => i !== userId) });
        newUser = Object.assign(Object.assign({}, existUser[0]._doc), { [vote]: (_b = existUser[0][vote]) === null || _b === void 0 ? void 0 : _b.filter((i) => i !== _id) });
    }
    else {
        const voteArrMovie = existMovie[0][vote];
        voteArrMovie === null || voteArrMovie === void 0 ? void 0 : voteArrMovie.push(userId);
        newMovie = Object.assign(Object.assign({}, existMovie[0]._doc), { [vote]: voteArrMovie });
        const voteArrUser = existUser[0][vote];
        voteArrUser === null || voteArrUser === void 0 ? void 0 : voteArrUser.push(_id);
        newUser = Object.assign(Object.assign({}, (_c = existUser[0]) === null || _c === void 0 ? void 0 : _c._doc), { [vote]: voteArrUser });
    }
    const updateUser = yield UserModel_1.UserModel.findByIdAndUpdate({ _id: userId }, newUser, { new: true });
    const updateMovie = yield MovieModel_1.MovieModel.findByIdAndUpdate({ _id }, newMovie, { new: true });
    res.status(200).json((0, resCustom_1.resCustom)(200, { data: { user: updateUser, movie: updateMovie } }, 'Rated movie successfully'));
});
exports.rateMovie = rateMovie;
const createMovie = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name, created_at, poster, like, dislike } = req.body;
        const existMovie = yield MovieModel_1.MovieModel.findOne({ name });
        if (existMovie) {
            return res.status(500).json((0, resCustom_1.resCustom)(500, { data: [] }, 'Movie already exists'));
        }
        const movieSubmit = {
            name,
            created_at: (0, functions_1.formatISODate)(created_at),
            poster,
            like,
            dislike,
        };
        const reqMovie = new MovieModel_1.MovieModel(movieSubmit);
        yield reqMovie.save();
        const newMovie = yield MovieModel_1.MovieModel.find();
        res.status(200).json((0, resCustom_1.resCustom)(200, { data: newMovie }, 'Created movie successfully'));
    }
    catch (err) {
        res.status(500).json(err);
    }
});
exports.createMovie = createMovie;

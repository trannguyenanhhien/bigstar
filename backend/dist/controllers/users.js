"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.refreshToken = exports.signIn = exports.signUp = void 0;
const resCustom_1 = require("../helpers/resCustom");
const UserModel_1 = require("../models/UserModel");
const argon2_1 = __importDefault(require("argon2"));
const auth_1 = require("../helpers/auth");
const signUp = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name, email, username, password } = req.body;
        const existUser = yield UserModel_1.UserModel.findOne({ username });
        if (existUser) {
            return res.status(500).json((0, resCustom_1.resCustom)(500, { data: [] }, 'Account already exists'));
        }
        const hashedPassword = yield argon2_1.default.hash(password);
        const userSubmit = {
            username,
            password: hashedPassword,
            name,
            email,
        };
        const reqUser = new UserModel_1.UserModel(userSubmit);
        yield reqUser.save();
        const newUser = yield UserModel_1.UserModel.find();
        res.status(200).json((0, resCustom_1.resCustom)(200, { data: newUser }, 'Created account successfully'));
    }
    catch (err) {
        res.status(500).json(err);
    }
});
exports.signUp = signUp;
const signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // try {
    const { username, password } = req.body;
    const existUser = yield UserModel_1.UserModel.findOne({ username });
    if (!existUser) {
        return res.status(500).json((0, resCustom_1.resCustom)(500, {}, 'Not found account'));
    }
    const isPasswordValid = yield argon2_1.default.verify(existUser.password, password);
    if (!isPasswordValid) {
        return res.status(400).json((0, resCustom_1.resCustom)(400, {}, 'Wrong password'));
    }
    const resUser = {
        profile: {
            _id: existUser._id,
            username: existUser.username,
            name: existUser.name,
            email: existUser.email,
        },
        access: (0, auth_1.createToken)(existUser),
        refresh: (0, auth_1.createRefreshToken)(existUser),
    };
    return res.status(200).json((0, resCustom_1.resCustom)(200, { data: resUser }, 'Log in successfully'));
    // } catch (err) {
    //   res.status(500).json(err);
    // }
});
exports.signIn = signIn;
const refreshToken = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const user = (_a = (0, auth_1.checkAuthRefresh)(req, res)) === null || _a === void 0 ? void 0 : _a.user;
        const resUser = {
            data: {
                profile: {
                    _id: user === null || user === void 0 ? void 0 : user._id,
                    username: user === null || user === void 0 ? void 0 : user.username,
                    name: user === null || user === void 0 ? void 0 : user.name,
                    email: user === null || user === void 0 ? void 0 : user.email,
                },
                access: (0, auth_1.createToken)(user),
                refresh: (0, auth_1.createRefreshToken)(user),
            },
        };
        return res.status(200).json((0, resCustom_1.resCustom)(200, { data: resUser }, 'Recreate token successfully'));
    }
    catch (error) {
        res.status(500).json(error);
    }
});
exports.refreshToken = refreshToken;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const movies_js_1 = require("../controllers/movies.js");
const router = express_1.default.Router();
router.get('/list', movies_js_1.getMovies);
router.put('/rate', movies_js_1.rateMovie);
router.post('/create', movies_js_1.createMovie);
exports.default = router;

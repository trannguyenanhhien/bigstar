"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const users_js_1 = require("../controllers/users.js");
const router = express_1.default.Router();
router.post('/signup', users_js_1.signUp);
router.post('/signin', users_js_1.signIn);
router.post('/refresh', users_js_1.refreshToken);
exports.default = router;

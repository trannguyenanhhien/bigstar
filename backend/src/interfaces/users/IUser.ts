export interface IUser {
  _id: String;
  name: String;
  username: String;
  password: String;
  email: String;
}

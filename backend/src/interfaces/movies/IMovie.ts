export interface IMovie {
  _id: string;
  name: string;
  poster: string;
  created_at: Date;
  like: string;
  dislike: string;
}

import express from 'express'
import { getMovies, rateMovie, createMovie } from '../controllers/movies.js'
const router = express.Router()

router.get('/list', getMovies)
router.put('/rate', rateMovie)
router.post('/create', createMovie)

export default router

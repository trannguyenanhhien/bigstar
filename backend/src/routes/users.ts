import express from 'express'
import { signIn, signUp, refreshToken } from '../controllers/users.js'
const router = express.Router()

router.post('/signup', signUp)
router.post('/signin', signIn)
router.post('/refresh', refreshToken)

export default router

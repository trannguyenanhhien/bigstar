import express, {Express, NextFunction, Request, Response} from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import {checkAuth} from './helpers/auth';
import mongoose from 'mongoose';
import {URI} from './configs/database';
import bodyParser from 'body-parser';
import users from './routes/users';
import movies from './routes/movies'

dotenv.config();

const port = process.env.PORT;

const app: Express = express();

app.use(bodyParser.json({limit: '30mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '30mb'}));
const allowedOrigins = ['http://localhost:3000'];
const corsOptions: cors.CorsOptions = {
  origin: allowedOrigins,
};
app.use(cors(corsOptions));
app.use('/users', users);
const middlewware = (req: Request, res: Response, next: NextFunction) => {
  checkAuth(req, res, next);
};

app.use('/movies', movies);
app.use(middlewware);
// mongoose.set('strictQuery', false);
mongoose
  .connect(URI, {useNewURLParser: true, useUnifiedTopology: true} as Object)
  .then(() => {
    console.log('Connect to DB');
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  })
  .catch((err) => {
    console.log('err', err);
  });

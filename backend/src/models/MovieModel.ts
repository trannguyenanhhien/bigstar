import mongoose from 'mongoose';

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
  },
  poster: {
    type: String,
    required: true,
  },
  like: {
    type: Array,
    default: []
  },
  dislike: {
    type: Array,
    default: []
  },

});

export const MovieModel = mongoose.model('Movie', schema);

import mongoose from 'mongoose'

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
  },
  like: {
    type: Array
  },
  dislike: {
    type: Array
  }
})

export const UserModel = mongoose.model('User', schema)

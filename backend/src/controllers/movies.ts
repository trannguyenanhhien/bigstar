import {Request, Response} from 'express';
import {formatISODate} from '../helpers/functions';
import {resCustom} from '../helpers/resCustom';
import {IMovie} from '../interfaces/movies/IMovie';
import {IUser} from '../interfaces/users/IUser';
import {MovieModel} from '../models/MovieModel';
import {UserModel} from '../models/UserModel';

export const getMovies = async (req: Request, res: Response) => {
  try {
    const query: any = req.query;
    const length = (await MovieModel.find()).length;
    const movies = await MovieModel.find()
      .limit(query.page_size)
      .skip((query.page - 1) * query.page_size);
    const data = {
      data: movies,
      page_info: {
        current_limit: parseInt(query.page_size),
        current_page: parseInt(query.page),
        total: length,
      },
    };
    res.status(200).json(resCustom(200, {data: data.data, page_info: data.page_info}));
  } catch (error) {
    res.status(500).json(error);
  }
};

export const rateMovie = async (req: Request, res: Response) => {
  const {_id, vote, userId} = req.body;
  const existMovie: any = await MovieModel.find({_id: _id});
  const existUser: any = await UserModel.find({_id: userId});
  const votedUser: any = await UserModel.find({
    $and: [
      {_id: userId},
      {
        $or: [{[vote]: {$all: [_id]}}],
      },
    ],
  });

  let newMovie = existMovie[0];
  let newUser = existUser[0];
  if (votedUser.length) {
    newMovie = {
      ...existMovie[0]._doc,
      [vote]: existMovie[0][vote]?.filter((i: string) => i !== userId),
    };
    newUser = {
      ...existUser[0]._doc,
      [vote]: existUser[0][vote]?.filter((i: string) => i !== _id),
    };
  } else {
    const voteArrMovie = existMovie[0][vote];
    voteArrMovie?.push(userId);
    newMovie = {
      ...existMovie[0]._doc,
      [vote]: voteArrMovie,
    };
    const voteArrUser = existUser[0][vote];
    voteArrUser?.push(_id);
    newUser = {
      ...existUser[0]?._doc,
      [vote]: voteArrUser,
    };
  }
  const updateUser = await UserModel.findByIdAndUpdate({_id: userId}, newUser, {new: true});
  const updateMovie = await MovieModel.findByIdAndUpdate({_id}, newMovie, {new: true});
  res.status(200).json(resCustom(200, {data: {user: updateUser, movie: updateMovie}}, 'Rated movie successfully'));
};

export const createMovie = async (req: Request, res: Response) => {
  try {
    const {name, created_at, poster, like, dislike} = req.body;
    const existMovie = await MovieModel.findOne({name});
    if (existMovie) {
      return res.status(500).json(resCustom(500, {data: []}, 'Movie already exists'));
    }
    const movieSubmit = {
      name,
      created_at: formatISODate(created_at),
      poster,
      like,
      dislike,
    };
    const reqMovie = new MovieModel(movieSubmit);
    await reqMovie.save();
    const newMovie = await MovieModel.find();
    res.status(200).json(resCustom(200, {data: newMovie}, 'Created movie successfully'));
  } catch (err) {
    res.status(500).json(err);
  }
};

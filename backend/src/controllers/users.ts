import {resCustom} from '../helpers/resCustom';
import {UserModel} from '../models/UserModel';
import argon2 from 'argon2';
import {Request, Response} from 'express';
import {checkAuthRefresh, createRefreshToken, createToken} from '../helpers/auth';
export const signUp = async (req: Request, res: Response) => {
  try {
    const {name, email, username, password} = req.body;
    const existUser = await UserModel.findOne({username});
    if (existUser) {
      return res.status(500).json(resCustom(500, {data: []}, 'Account already exists'));
    }
    const hashedPassword = await argon2.hash(password);
    const userSubmit = {
      username,
      password: hashedPassword,
      name,
      email,
    };
    const reqUser = new UserModel(userSubmit);
    await reqUser.save();
    const newUser = await UserModel.find();
    res.status(200).json(resCustom(200, {data: newUser}, 'Created account successfully'));
  } catch (err) {
    res.status(500).json(err);
  }
};

export const signIn = async (req: Request, res: Response) => {
  // try {
    const {username, password} = req.body;
    const existUser = await UserModel.findOne({username});
    if (!existUser) {
      return res.status(500).json(resCustom(500, {}, 'Not found account'));
    }
    const isPasswordValid = await argon2.verify(existUser.password, password);
    if (!isPasswordValid) {
      return res.status(400).json(resCustom(400, {}, 'Wrong password'));
    }
    const resUser = {
      profile: {
        _id: existUser._id,
        username: existUser.username,
        name: existUser.name,
        email: existUser.email,
      },
      access: createToken(existUser),
      refresh: createRefreshToken(existUser),
    };
    return res.status(200).json(resCustom(200, {data: resUser}, 'Log in successfully'));
  // } catch (err) {
  //   res.status(500).json(err);
  // }
};

export const refreshToken = async (req: Request, res: Response) => {
  try {
    const user = checkAuthRefresh(req, res)?.user;
    const resUser = {
      data: {
        profile: {
          _id: user?._id,
          username: user?.username,
          name: user?.name,
          email: user?.email,
        },
        access: createToken(user),
        refresh: createRefreshToken(user),
      },
    };
    return res.status(200).json(resCustom(200, {data: resUser}, 'Recreate token successfully'));
  } catch (error) {
    res.status(500).json(error);
  }
};

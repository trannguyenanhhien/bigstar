interface IData extends Object {
  data?: Object | Array<any>;
  page_info?: Object;
}

export const resCustom = (status: Number, data?: IData, message?: String) => {
  return {
    success: status === 200 ? true : false,
    message: message,
    status_code: status,
    data: data?.data,
    page_info: data?.page_info,
  };
};

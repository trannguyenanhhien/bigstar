/* eslint-disable no-useless-escape */
// const moment = require('moment')
import moment from 'moment';

function formatMoney(n: number) {
  return (Math.round(n * 100) / 100).toLocaleString();
}
function formatDate(date: any, char = '/') {
  return moment(date).format(`D${char}M${char}YYYY`);
}
function formatDateData(date: any, char = '/') {
  return moment(date).format(`YYYY${char}MM${char}DD`);
}
function formatDateTime(date: any, char = '/') {
  return moment(date).format(`D${char}M${char}YYYY hh:mm:ss`);
}
function formatDateCustom(date: any, format: string) {
  return moment(date).format(format);
}
function formatISODate(date: any) {
  return moment.utc(moment(date).format('YYYY-MM-DD'));
}
function currentDate() {
  const date = new Date();
  return date;
}
function compareDate(a: any, b: any) {
  const date1 = new Date(a).getTime();
  const date2 = new Date(b).getTime();
  if (date1 > date2) return 1;
  else if (date1 < date2) return 2;
  else return 0;
}
function strToSlug(title: string) {
  //Đổi chữ hoa thành chữ thường
  let slug = title.toLowerCase();

  //Đổi ký tự có dấu thành không dấu
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');
  //Xóa các ký tự đặt biệt
  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
  //Đổi khoảng trắng thành ký tự gạch ngang
  slug = slug.replace(/ /gi, '-');
  //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');
  //Xóa các ký tự gạch ngang ở đầu và cuối
  slug = '@' + slug + '@';
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');
  return slug;
}

// async function checkToken() {
//   let currentDate = new Date()
//   const token = localStorage.getItem('authToken')
//   if (token) {
//     const decode = await jwt_decode(token)
//     if (decode.exp * 1000 >= currentDate.getTime()) {
//       return true
//     }
//   }

//   return false
// }

// const getTokenH = () => {
//   const Token = localStorage.getItem('authToken')
//   return Token
// }

// const getRefresh = () => {
//   const RefreshToken = state.authSlice.auth.refresh;
//   return RefreshToken;
// };

// const getProfile = () => {
//   const Profile = JSON.parse(localStorage.getItem("profile"));
//   return Profile;
// };

const capitalize = (text: string) => {
  return text
    .toLowerCase()
    .split(' ')
    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ');
};

// const keyOnData = (data) => {
//   const arr = []
//   data.forEach((el, index) => {
//     const item = {
//       key: index,
//       ...el,
//     }
//     arr.push(item)
//   })
//   return arr
// }

// const getFileURL = (img) => {
//   return img ? API_URL + img?.substring(1) : null
// }

// const getFileFromURL = async (url) => {
//   try {
//     const res = await fetch(url)
//     const blob = await res.blob()
//     console.log(res)
//     const fileName = url.slice(url.lastIndexOf('/') + 1, -1)

//     return new File([blob], fileName)
//   } catch (error) {
//     throw error
//   }
// }

export {
  formatMoney,
  formatDate,
  formatDateData,
  formatDateTime,
  formatDateCustom,
  currentDate,
  compareDate,
  // strToSlug,
  // checkToken,
  // getTokenH,
  // getRefresh,
  // getProfile,
  capitalize,
  // keyOnData,
  // getFileURL,
  // getFileFromURL,
  // formatCurrency,
  formatISODate,
};

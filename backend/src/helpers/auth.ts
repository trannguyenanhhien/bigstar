import jwt, {Secret, JwtPayload, verify} from 'jsonwebtoken';
import {Request, Response, NextFunction} from 'express';
import dotenv from 'dotenv';
import {resCustom} from './resCustom';
import {IUser} from '../interfaces/users/IUser';
dotenv.config();

const secretKey: Secret | any = process.env.ACCESS_TOKEN_SECRET;
const refreshSecretKey: Secret | any = process.env.REFRESH_TOKEN_SECRET;

export const createToken = (user: any) => {
  return jwt.sign({userId: user._id}, secretKey, {
    expiresIn: '150m',
  });
};

export const createRefreshToken = (user: any) => {
  return jwt.sign({userId: user._id}, refreshSecretKey, {
    expiresIn: '15000m',
  });
};

export const checkAuth = (req: Request, res: Response, next: NextFunction) => {
  try {
    const authHeader = req.header('Authorization');
    const accessToken: any = authHeader && authHeader.split(' ')[1];
    if (!accessToken) res.status(401).json(resCustom(401, {}, 'Token expired'));
    const decodedUser = verify(accessToken, secretKey);
    if (decodedUser) return next();
    else res.status(401).json(resCustom(401, {}, 'Token expired'));
  } catch (error) {
    throw res.status(500).json({mess: 'Lỗi rồi 123'});
  }
};

interface IJwtPayload extends jwt.JwtPayload {
  user?: IUser;
}

export const checkAuthRefresh = (req: Request, res: Response) => {
  try {
    const tokenReq = req.body.refresh;
    const refreshToken = tokenReq && tokenReq.split(' ')[1];
    if (!refreshToken) res.status(401).json(resCustom(401, {}, 'Refresh token expired'));
    const decodedUser = jwt.verify(refreshToken, refreshSecretKey as jwt.Secret);
    if (decodedUser) return decodedUser as IJwtPayload;
    else res.status(401).json(resCustom(401, {}, 'Refresh token expired'));
  } catch (error) {
    res.status(500).json({mess: 'Lỗi rồi 12'});
  }
};

import React from 'react'
import { Input } from '@chakra-ui/react'

const InputCustom = ({ type, placeholder, name, ...props }) => {
  return (
    <Input
      type={type}
      placeholder={placeholder}
      bg={'gray.100'}
      border={0}
      color={'black.500'}
      _placeholder={{
        color: 'gray.500',
      }}
      name={name}
      {...props}
    />
  )
}

export default React.memo(InputCustom)

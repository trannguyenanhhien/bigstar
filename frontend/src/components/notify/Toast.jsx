import { Button, useToast, Wrap, WrapItem } from '@chakra-ui/react'
import React from 'react'

const Toast = (props) => {
  const toast = useToast()
  const { title, status = 'success', isClosable = true, position = 'top' } = props.toast
  const { toastRef } = props
  return (
    <>
      <Wrap>
        <WrapItem>
          <Button
            ref={toastRef}
            onClick={() =>
              toast({
                title,
                position,
                isClosable,
                status,
              })
            }
          >
            Show {position} toast
          </Button>
        </WrapItem>
      </Wrap>
    </>
  )
}

export default React.memo(Toast)

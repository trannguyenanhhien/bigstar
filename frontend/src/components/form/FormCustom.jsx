import React from 'react'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import { FormControl, FormErrorMessage } from '@chakra-ui/react'
import InputCustom from 'components/inputs/InputCustom'

const FormCustom = (props) => {
  const { schema, validationSchema, onSubmit, initialValues, formikRef } = props
  return (
    <>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {({ errors, touched }) => (
          <Form>
            {schema.map((item) => (
              <Field name={item.name}>
                {({ field, form }) => (
                  <FormControl p={2} isInvalid={form.errors[item.name] && form.touched[item.name]}>
                    <InputCustom {...field} type={item.type} name={item.name} placeholder={item.placeholder} />
                    {errors[item.name] && touched[item.name] ? <FormErrorMessage>{errors[item.name]}</FormErrorMessage> : null}
                  </FormControl>
                )}
              </Field>
            ))}
            <button type="submit" style={{ display: 'none' }} ref={formikRef} />
          </Form>
        )}
      </Formik>
    </>
  )
}

export default React.memo(FormCustom)

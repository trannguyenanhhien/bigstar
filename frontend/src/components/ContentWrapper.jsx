import { Box, Heading } from '@chakra-ui/react'
import React from 'react'

const ContentWrapper = (props) => {
  const { title, children } = props
  return (
    <Box p={'3% 10%'}>
      <Heading size={'lg'}>{title}</Heading>
      {children}
    </Box>
  )
}

export default ContentWrapper

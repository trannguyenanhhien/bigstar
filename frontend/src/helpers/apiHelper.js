import queryString from 'query-string'
import { getToken } from './functions'

const profile = JSON.parse(localStorage.getItem('profile'))

export const withQuery = (url, query) => {
  return queryString.stringifyUrl({ url, query }, { arrayFormat: 'comma' })
}

export const xAuthToken = (params) => {
  var headers
  if (params) {
    if (params.downloadBarcode === true) {
      headers = {
        headers: {
          Authorization: `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
          'x-app-id': 'P_STORE',
        },
        responseType: 'blob',
      }
    } else if (params.xUsername) {
      headers = {
        // 'X-Username': `${getProfile().user_name}`,
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
        'x-app-id': 'P_STORE',
      }
    }
  } else {
    if (getToken()) {
      headers = {
        headers: {
          Authorization: `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
          'x-app-id': 'P_STORE',
        },
      }
    }
  }
  return headers
}

export function xAuthConnectUsb(formData = null, isJson = null) {
  let contentType = 'application/x-www-form-urlencoded'
  if (formData) {
    contentType = 'multipart/form-data'
  }
  if (isJson) {
    contentType = 'application/json'
  }
  const headers = {
    headers: {
      'Content-Type': contentType,
      // 'Access-Control-Allow-Origin': '*'
    },
    responseType: 'blob',
  }
  return headers
}

export function xAuthTokenP(formData = null, isJson = null) {
  let contentType = 'application/x-www-form-urlencoded'
  if (formData) {
    contentType = 'multipart/form-data'
  }
  if (isJson) {
    contentType = 'application/json'
  }
  if (profile) {
    const company_id = profile.company.company_id
    const headers = {
      headers: {
        'X-Access-Token': getToken(),
        'Content-Type': contentType,
        'X-Company-Id': company_id,
        'x-app-id': 'P_STORE',
      },
    }
    return headers
  }
}

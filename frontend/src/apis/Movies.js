// import axiosInstance from "./api";
import axios from 'axios'
import { withQuery, xAuthToken } from 'helpers/apiHelper'
import { GET_MOVIES, VOTED_MOVIE } from './constants'

const getMovies = (params, res, err) => {
  return axios.get(withQuery(GET_MOVIES, params), xAuthToken()).then(res).catch(err)
}
const rateMovie = (data, res, err) => {
  return axios.put(VOTED_MOVIE, data, xAuthToken()).then(res).catch(err)
}

export { getMovies, rateMovie }

import axios from 'axios'
import { LOG_IN, SIGN_UP } from './constants'

const signup = (data, res, err) => {
  return axios.post(SIGN_UP, data).then(res).catch(err)
}
const login = (data, res, err) => {
  return axios.post(LOG_IN, data).then(res).catch(err);
}

export { signup, login }

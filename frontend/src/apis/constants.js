const API_URL = process.env.REACT_APP_API_URL

const SIGN_UP = API_URL + 'users/signup'
const LOG_IN = API_URL + 'users/signin'

const GET_MOVIES = API_URL + 'movies/list'
const VOTED_MOVIE = API_URL + 'movies/rate'

export { SIGN_UP, LOG_IN, GET_MOVIES, VOTED_MOVIE }

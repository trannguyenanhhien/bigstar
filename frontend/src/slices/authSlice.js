import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  auth: {
    access: '',
    refresh: '',
    profile: '',
  },
};
const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action) => {

      console.log(action.payload);
      state.auth.access = action.payload.access;
      state.auth.refresh = action.payload.refresh;
      state.auth.profile = action.payload.profile;
    },
    logout: (state) => {
      state.auth.access = '';
      state.auth.refresh = '';
      state.auth.profile = '';
    },
  },
});

const {actions, reducer} = authSlice;

export const {login, logout} = actions;

export default reducer;

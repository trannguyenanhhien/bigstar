import { Box, Center, Image, Flex, Text, Stack, useColorModeValue, CircularProgress, CircularProgressLabel, color } from '@chakra-ui/react'
import likeIcon from 'assets/icons/like.png'
import { formatDateCustom } from 'helpers/functions'
import { useSelector } from 'react-redux'

export default function MovieCard(props) {
  const { poster, name, created_at, like, dislike } = props.data
  const profile = useSelector((state) => state.authSlice.auth.profile)
  const liked = like?.filter((i) => i === profile._id)
  const disliked = dislike?.filter((i) => i === profile._id)
  const { onAction } = props
  const likeQuantity = like?.length
  const dislikeQuantity = dislike?.length
  const percent = (+likeQuantity / (+likeQuantity + +dislikeQuantity)).toFixed(2) * 100
  return (
    <Center py={3}>
      <Box
        maxW={{ base: '250px', sm: '150px', md: '180px' }}
        w={'full'}
        h={{ base: '520px', sm: '380px', md: '420px', lg: '430px' }}
        bg={useColorModeValue('white', 'gray.800')}
        boxShadow={'xl'}
        rounded={'md'}
        overflow={'hidden'}
        style={{ position: 'relative' }}
      >
        <Image w={'full'} src={poster} objectFit={'cover'} cursor="pointer" />
        <Flex justify={'left'} mt={-6} ml={2}>
          <Box bg={'black'} borderRadius={30} border={'2px solid white'}>
            <CircularProgress
              value={percent ? percent : 0}
              color={percent >= 70 ? 'green.400' : percent < 50 ? 'red.400' : 'orange.300'}
              borderRadius={30}
              thickness="6px"
              size={'40px'}
            >
              <CircularProgressLabel color={'white'} fontWeight={800}>
                {likeQuantity && dislikeQuantity ? percent : 'NR'}
                {likeQuantity && dislikeQuantity ? <span style={{ fontSize: 7 }}>%</span> : ''}
              </CircularProgressLabel>
            </CircularProgress>
          </Box>
        </Flex>
        <Box p={2}>
          <Stack spacing={0} align={'left'} mb={3}>
            <Text fontWeight={700} _hover={{ color: '#05B4E4' }} cursor="pointer">
              {name}
            </Text>
            <Text color={'gray.500'} fontSize={14}>
              {formatDateCustom(created_at, 'MMM DD, YYYY')}
            </Text>
          </Stack>
          <Stack direction={'row'} justify={'center'} spacing={6} style={{ position: 'absolute', bottom: 10, left: 0, right: 0 }}>
            <Stack spacing={0} align={'center'}>
              <Image src={likeIcon} width={'30px'} cursor="pointer" onClick={() => onAction('like')} />
              <Text fontSize={'sm'} fontWeight={liked?.length && 600} color={liked?.length ? '#05B4E4' : 'gray.500'}>
                {likeQuantity} like<span>{likeQuantity > 1 && 's'}</span>
              </Text>
            </Stack>
            <Stack spacing={0} align={'center'}>
              <Image src={likeIcon} width={'30px'} transform={'rotate(180deg)'} cursor="pointer" onClick={() => onAction('dislike')} />
              <Text fontSize={'sm'} fontWeight={disliked?.length && 600} color={disliked?.length ? '#05B4E4' : 'gray.500'}>
                {dislikeQuantity} dislike<span>{dislikeQuantity > 1 && 's'}</span>
              </Text>
            </Stack>
          </Stack>
        </Box>
      </Box>
    </Center>
  )
}

import { Box, SimpleGrid, Spinner } from '@chakra-ui/react'
import ContentWrapper from 'components/ContentWrapper'
import { useEffect, useState } from 'react'
import Footer from './components/Footer'
import MovieCard from './components/MovieCard'
import { getMovies, rateMovie } from 'apis/Movies'
import { useOutletContext } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroll-component'
import { useSelector } from 'react-redux'

const Home = () => {
  const [onToast] = useOutletContext()
  const [listMovies, setListMovies] = useState([])
  const [hasMore, setHasMore] = useState(true)
  const profile = useSelector((state) => state.authSlice.auth.profile)
  const [params, setParams] = useState({
    page_size: 5,
    page: 1,
  })
  const getListMovies = async () => {
    await getMovies(params, (res) => {
      if (res.data.success) {
        setListMovies(res.data.data)
      }
    })
  }
  useEffect(() => {
    getListMovies()
    document.title = 'Big Star - Home'
  }, [])
  const fetchMoreMovies = () => {
    setParams({
      ...params,
      page: params.page + 1,
    })
    if (listMovies.length === 15) {
      setHasMore(false)
      setParams({
        ...params,
        page: params.page - 1,
      })
    } else {
      setTimeout(() => {
        getMovies(
          {
            ...params,
            page: params.page + 1,
          },
          (res) => {
            if (res.data.success) {
              setListMovies(listMovies.concat(res.data.data))
            }
          }
        )
      }, 500)
    }
  }
  const onAction = (action, item) => {
    if (action.includes('like')) {
      const data = {
        _id: item._id,
        vote: action,
        userId: profile._id,
      }
      onSubmit(action, data)
    }
  }
  const onSubmit = async (action, data) => {
    rateMovie(
      data,
      async (res) => {
        if (res.data.success) {
          // console.log(res.data.data)
          const data = res.data.data.movie
          const indexInList = listMovies.findIndex((item) => item._id === data._id)
          listMovies[indexInList] = data
          onToast({ title: 'Voted movie successfully', status: 'success' })
        }
      },
      (err) => {}
    )
  }
  return (
    <>
      <ContentWrapper title="Home">
        <InfiniteScroll
          dataLength={listMovies.length}
          next={fetchMoreMovies}
          hasMore={hasMore}
          loader={
            <div style={{ textAlign: 'center' }}>
              <Spinner />
            </div>
          }
          endMessage={<div style={{ textAlign: 'center' }}>This is all movies</div>}
        >
          <SimpleGrid columns={{ base: 1, sm: 2, md: 3, lg: 5 }} spacing={2}>
            {listMovies.map((item, index) => (
              <MovieCard data={item} onAction={(check) => onAction(check, item)} />
            ))}
          </SimpleGrid>
        </InfiniteScroll>
      </ContentWrapper>
      <Footer />
    </>
  )
}

export default Home

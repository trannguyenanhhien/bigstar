import React, { useRef, useState } from 'react'
import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  Stack,
  Collapse,
  Icon,
  Link,
  Popover,
  PopoverTrigger,
  PopoverContent,
  useColorModeValue,
  useDisclosure,
  Image,
  Show,
} from '@chakra-ui/react'
import { HamburgerIcon, CloseIcon, ChevronDownIcon, Search2Icon } from '@chakra-ui/icons'
import { Outlet } from 'react-router-dom'
import SearchInput from 'components/inputs/SearchInput'
import Logo from 'assets/images/logo.png'
import Toast from 'components/notify/Toast'
import { useSelector } from 'react-redux'

const MainLayout = (props) => {
  const { isOpen, onToggle } = useDisclosure()
  const [openSearch, setOpenSearch] = useState(false)
  const profile = useSelector((state) => state.authSlice.auth.profile)
  const [toast, setToast] = useState({
    title: '',
    status: '',
  })
  const toastRef = useRef(null)
  const onSearch = () => {
    setOpenSearch(!openSearch)
  }
  const onToast = ({ title, status }) => {
    setToast({
      ...toast,
      title,
      status,
    })
    setTimeout(() => {
      toastRef.current.click()
    }, 0)
  }
  return (
    <>
      <Box>
        <Box style={{ display: 'none' }}>
          <button onClick={() => toastRef.current.click()}>Click</button>
          <Toast toastRef={toastRef} toast={toast} />
        </Box>
        <Flex
          bg={'#032541'}
          color={useColorModeValue('gray.600', 'white')}
          minH={'60px'}
          py={{ base: 2 }}
          px={{ base: 4 }}
          borderBottom={1}
          borderStyle={'solid'}
          borderColor={useColorModeValue('gray.200', 'gray.900')}
          align={'center'}
        >
          <Flex flex={{ base: 1, md: 'auto' }} ml={{ base: -2 }} display={{ base: 'flex', md: 'none' }}>
            <IconButton
              color={'white'}
              _hover={{
                color: 'black',
                backgroundColor: 'white',
              }}
              onClick={onToggle}
              icon={isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />}
              variant={'ghost'}
              aria-label={'Toggle Navigation'}
            />
          </Flex>
          <Flex flex={{ base: 2 }} justify={{ base: 'center', md: 'start' }} alignItems="center">
            <Image src={Logo} width="120px" />
            <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
              <DesktopNav />
            </Flex>
          </Flex>

          <Stack flex={{ base: 1, md: 1 }} justify={'flex-end'} direction={'row'} spacing={6}>
            <Show above="md">
              <Text style={{ textAlign: 'right', color: 'white' }} mr={3}>
                Welcome <div style={{ fontWeight: 600, fontSize: 17 }}>{profile.name}</div>
              </Text>
            </Show>
          </Stack>
        </Flex>
        <Collapse in={isOpen} animateOpacity>
          <MobileNav />
        </Collapse>
        <Box>
          {openSearch && <SearchInput />}
          <Outlet context={[onToast]} />
        </Box>
      </Box>
    </>
  )
}
const DesktopNav = () => {
  const linkColor = 'white'
  const linkHoverColor = 'white'
  const popoverContentBgColor = useColorModeValue('white', 'gray.800')

  return (
    <Stack direction={'row'} spacing={4}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={'hover'} placement={'bottom-start'}>
            <PopoverTrigger>
              <Link
                p={2}
                href={navItem.href ?? '#'}
                fontSize={'md'}
                fontWeight={500}
                color={linkColor}
                _hover={{
                  textDecoration: 'none',
                  color: linkHoverColor,
                }}
              >
                {navItem.label}
              </Link>
            </PopoverTrigger>

            {navItem.children && (
              <PopoverContent border={0} boxShadow={'xl'} bg={popoverContentBgColor} p={3} rounded={'xl'} minW={'xs'}>
                <Stack>
                  {navItem.children.map((child) => (
                    <DesktopSubNav key={child.label} {...child} />
                  ))}
                </Stack>
              </PopoverContent>
            )}
          </Popover>
        </Box>
      ))}
    </Stack>
  )
}

const DesktopSubNav = ({ label, href }) => {
  return (
    <Link href={href} role={'group'} display={'block'} p={1} rounded={'md'} _hover={{ bgGradient: 'linear(to-r, teal.50, green.50)' }}>
      <Stack direction={'row'} align={'center'}>
        <Box>
          <Text bgGradient="linear(to-r, teal.500, green.500)" bgClip="text" transition={'all .3s ease'} fontWeight={500}>
            {label}
          </Text>
        </Box>
      </Stack>
    </Link>
  )
}

const MobileNav = () => {
  const profile = useSelector((state) => state.authSlice.auth.profile)
  return (
    <Stack bg={useColorModeValue('white', 'gray.800')} p={4} display={{ md: 'none' }}>
      <div>
        <Show above="sm">
          <Text style={{ textAlign: 'center', color: 'black' }} mr={3}>
            Welcome <div style={{ fontWeight: 600, fontSize: 15 }}>{profile.name}</div>
          </Text>
        </Show>
      </div>
      {NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  )
}

const MobileNavItem = ({ label, children, href }) => {
  const { isOpen, onToggle } = useDisclosure()

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Flex
        py={2}
        as={Link}
        href={href ?? '#'}
        justify={'space-between'}
        align={'center'}
        _hover={{
          textDecoration: 'none',
        }}
      >
        <Text fontWeight={600} color={useColorModeValue('gray.600', 'gray.200')}>
          {label}
        </Text>
        {children && <Icon as={ChevronDownIcon} transition={'all .25s ease-in-out'} transform={isOpen ? 'rotate(180deg)' : ''} w={6} h={6} />}
      </Flex>

      <Collapse in={isOpen} animateOpacity style={{ marginTop: '0!important' }}>
        <Stack mt={2} pl={4} borderLeft={1} borderStyle={'solid'} borderColor={useColorModeValue('gray.200', 'gray.700')} align={'start'}>
          {children &&
            children.map((child) => (
              <Link key={child.label} py={2} href={child.href}>
                {child.label}
              </Link>
            ))}
        </Stack>
      </Collapse>
    </Stack>
  )
}

const NAV_ITEMS = [
  {
    label: 'Movie',
    children: [
      {
        label: 'Popular',
        href: '#',
      },
      {
        label: 'Upcoming',
        href: '#',
      },
      {
        label: 'Top Rate',
        href: '#',
      },
    ],
  },
  {
    label: 'TV Show',
    href: '#',
  },
  {
    label: 'People',
    href: '#',
  },
  {
    label: 'More',
    href: '#',
  },
]
export default MainLayout

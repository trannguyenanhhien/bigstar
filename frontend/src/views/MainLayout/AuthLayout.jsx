import React, { useRef, useState } from 'react'
import { Box } from '@chakra-ui/react'
import { Outlet } from 'react-router-dom'
import Toast from 'components/notify/Toast'

const AuthLayout = (props) => {
  const [toast, setToast] = useState({
    title: '',
    status: '',
  })
  const toastRef = useRef(null)
  const onToast = ({ title, status }) => {
    setToast({
      ...toast,
      title,
      status,
    })
    setTimeout(() => {
      toastRef.current.click()
    }, 0)
  }
  return (
    <>
      <Box>
        <Box style={{ display: 'none' }}>
          <button onClick={() => toastRef.current.click()}>Click</button>
          <Toast toastRef={toastRef} toast={toast} />
        </Box>
        <Box>
          <Outlet context={[onToast]} />
        </Box>
      </Box>
    </>
  )
}
export default AuthLayout

import FormCustom from 'components/form/FormCustom'
import React from 'react'
import * as Yup from 'yup'

const LoginForm = (props) => {
  const { onSubmit, formikRef } = props
  const schema = React.useMemo(
    () => [
      { type: 'text', name: 'username', placeholder: 'Username or Email' },
      { type: 'password', name: 'password', placeholder: 'Password' },
    ],
    []
  )
  const initialValues = {
    username: '',
    password: '',
  }
  const loginSchema = Yup.object().shape({
    username: Yup.string().required('Required'),
    password: Yup.string().required('Required'),
  })
  return (
    <>
      <FormCustom initialValues={initialValues} validationSchema={loginSchema} onSubmit={onSubmit} schema={schema} formikRef={formikRef} />
    </>
  )
}

export default React.memo(LoginForm)

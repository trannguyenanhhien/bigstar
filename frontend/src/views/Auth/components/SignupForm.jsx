import FormCustom from 'components/form/FormCustom'
import React from 'react'
import * as Yup from 'yup'

const SignupForm = (props) => {
  const { onSubmit, formikRef } = props
  const schema = React.useMemo(
    () => [
      { type: 'text', name: 'name', placeholder: 'Name' },
      { type: 'text', name: 'username', placeholder: 'Username' },
      { type: 'text', name: 'email', placeholder: 'Email' },
      { type: 'password', name: 'password', placeholder: 'Password' },
    ],
    []
  )
  const initialValues = {
    name: '',
    username: '',
    email: '',
    password: '',
  }
  const signupSchema = Yup.object().shape({
    name: Yup.string().min(5, 'Too Short!').max(50, 'Too Long!').required('Required'),
    username: Yup.string().min(5, 'Too Short!').max(50, 'Too Long!').required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    password: Yup.string().required('Required'),
  })
  return (
    <>
      <FormCustom initialValues={initialValues} validationSchema={signupSchema} onSubmit={onSubmit} schema={schema} formikRef={formikRef} />
    </>
  )
}

export default React.memo(SignupForm)

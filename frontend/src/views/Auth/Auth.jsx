import { Box, Flex, Stack, Heading, Text, Container, Button, SimpleGrid, useBreakpointValue, Icon } from '@chakra-ui/react'
import { login, signup } from 'apis/Auth'
import { useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useNavigate, useOutletContext } from 'react-router-dom'
import LoginForm from './components/LoginForm'
import SignupForm from './components/SignupForm'
import { login as loginSlice } from 'slices/authSlice'

export default function Auth() {
  const navigate = useNavigate()
  const [onToast] = useOutletContext()
  const location = useLocation()
  const dispatch = useDispatch()
  useEffect(() => {
    document.title = `Big Star - ${location.pathname === '/signup' ? 'Sign up' : 'Log in'}`
  }, [location.pathname])
  const onAction = (action, item) => {
    if (action === 'signup') onSubmit('signup', item)
    if (action === 'login') onSubmit('login', item)
  }
  const onSubmit = (action, item) => {
    if (action === 'signup')
      signup(
        item,
        (res) => {
          if (res.data.success) {
            onToast({ title: 'Created account successfully', status: 'success' })
            setTimeout(() => {
              navigate('/')
            }, 1000)
          }
        },
        (err) => {
          onToast({ title: 'Created account unsuccessfully', status: 'error' })
        }
      )

    if (action === 'login')
      login(
        item,
        (res) => {
          if (res.data.success) {
            onToast({ title: 'Log in successfully', status: 'success' })
            setTimeout(() => {
              dispatch(loginSlice(res.data.data))
            }, 1000)
          }
        },
        (err) => {
          onToast({ title: 'Log in unsuccessfully', status: 'error' })
        }
      )
  }
  const formikRef = useRef()
  return (
    <Box position={'relative'}>
      <Container as={SimpleGrid} maxW={'7xl'} columns={{ base: 1, md: 2 }} spacing={{ base: 10, lg: 32 }} py={{ base: 10, sm: 20, lg: 32 }}>
        <Stack spacing={{ base: 10, md: 20 }}>
          <Heading zIndex={1} lineHeight={1.1} fontSize={{ base: '3xl', sm: '4xl', md: '5xl', lg: '6xl' }}>
            Welcome to Big Star
          </Heading>
        </Stack>
        <Stack bg={'gray.50'} rounded={'xl'} m={{ base: 0, sm: 6, md: 0 }} p={{ base: 4, sm: 5, md: 6 }} spacing={{ base: 8 }} maxW={{ lg: 'lg' }}>
          <Stack spacing={4}>
            <Heading color={'gray.800'} lineHeight={1.1} fontSize={{ base: '2xl', sm: '3xl', md: '4xl' }}>
              {location.pathname === '/signup' ? 'Sign up' : 'Log in'}
            </Heading>
          </Stack>
          <Box>
            {location.pathname === '/signup' ? (
              <SignupForm formikRef={formikRef} onSubmit={(values) => onAction('signup', values)} />
            ) : (
              <LoginForm formikRef={formikRef} onSubmit={(values) => onAction('login', values)} />
            )}
            <Flex mt={6} alignItems="center">
              <Box w={'40%'} ml={6} textAlign="left">
                {location.pathname === '/signup' ? (
                  <Text onClick={() => navigate('/')} color="blue" style={{ fontSize: 13, cursor: 'pointer', textDecoration: 'underline' }}>
                    Log in
                  </Text>
                ) : (
                  <Text color="blue" style={{ fontSize: 13, cursor: 'pointer', textDecoration: 'underline' }}>
                    Forgot password
                  </Text>
                )}
              </Box>
              <Box w={'60%'} mr={6} textAlign="right">
                <Button
                  onClick={() => {
                    formikRef.current.click()
                  }}
                  fontFamily={'heading'}
                  w={'80%'}
                  color={'white'}
                  _hover={{
                    bgGradient: 'linear(to-r, teal.500, green.400)',
                  }}
                  bgGradient="linear(to-r, teal.500, green.500)"
                >
                  {location.pathname === '/signup' ? 'Register' : 'Submit'}
                </Button>
              </Box>
            </Flex>
            {location.pathname === '/' && (
              <Text
                onClick={() => navigate('/signup')}
                color="blue"
                style={{ marginTop: 20, fontSize: 13, cursor: 'pointer', textAlign: 'center', textDecoration: 'underline' }}
              >
                Click here to register an account
              </Text>
            )}
          </Box>
        </Stack>
      </Container>
      <Blur position={'absolute'} top={-10} left={-10} style={{ filter: 'blur(70px)' }} />
    </Box>
  )
}

export const Blur = (props) => {
  return (
    <Icon
      width={useBreakpointValue({ base: '100%', md: '40vw', lg: '30vw' })}
      zIndex={useBreakpointValue({ base: -1, md: -1, lg: 0 })}
      height="560px"
      viewBox="0 0 528 560"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <circle cx="71" cy="61" r="111" fill="#F56565" />
      <circle cx="244" cy="106" r="139" fill="#ED64A6" />
      <circle cy="291" r="139" fill="#ED64A6" />
      <circle cx="80.5" cy="189.5" r="101.5" fill="#ED8936" />
      <circle cx="196.5" cy="317.5" r="101.5" fill="#ECC94B" />
      <circle cx="70.5" cy="458.5" r="101.5" fill="#48BB78" />
      <circle cx="426.5" cy="-0.5" r="101.5" fill="#4299E1" />
    </Icon>
  )
}

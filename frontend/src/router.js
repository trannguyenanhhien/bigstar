import { Navigate, Route, Routes, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import MainLayout from './views/MainLayout/MainLayout'
import Home from './views/Home/Home'
import Auth from 'views/Auth/Auth'
import AuthLayout from 'views/MainLayout/AuthLayout'

const PublicRouter = () => {
  return (
    <Routes>
      <Route element={<AuthLayout />}>
        <Route path="" element={<Auth />} />
        <Route path="signup" element={<Auth />} />
      </Route>
    </Routes>
  )
}
const PrivateRouter = () => {
  return (
    <Routes>
      <Route element={<MainLayout />}>
        <Route path="" element={<Home />} />
      </Route>
    </Routes>
  )
}
const Router = () => {
  const isLogin = useSelector((state) => state.authSlice.auth.access)
  if (!isLogin) {
    return <PublicRouter />
  } else {
    return <PrivateRouter />
  }
}

export default Router
